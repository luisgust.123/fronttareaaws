import { Encuesta } from '../_model/encuesta';
import { Injectable } from '@angular/core';
import { HOST_BACKEND } from '../_shared/constants';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {

  url: string = `${HOST_BACKEND}/api/encuestas`;

  mensajeRegistro = new Subject<string>();

  constructor(private httpClient: HttpClient) { }

  obtenerTodosLosRegistros() {
    return this.httpClient.get<Encuesta[]>(`${this.url}/listar`);
  }

  obtenerRegistros(id: number) {
    return this.httpClient.get<Encuesta>(`${this.url}/listarId/${id}`);
  }

  guardar(usuario: Encuesta) {
    return this.httpClient.post(`${this.url}/guardar`, usuario);
  }

  eliminar(id: number) {
    return this.httpClient.delete(`${this.url}/eliminar/${id}`);
  }
}
