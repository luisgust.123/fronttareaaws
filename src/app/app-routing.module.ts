import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { SecurityComponent } from './pages/security/security.component';
import { GuardService } from './_services/guard.service';
import { LogoutComponent } from './pages/logout/logout.component';
import { TiponegocioComponent } from './pages/admin/tiponegocio/adminEncuesta.component';
import { AdminComponent } from './pages/admin/admin/admin.component';
import { BodyComponent } from './pages/body/body.component';
import { LoginComponent } from './pages/login/login.component';
import { EncuestaComponent } from './pages/encuesta/encuesta.component';
import { ModificaEncuesta } from './pages/admin/tiponegocio/modificar/modificar.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [GuardService]},
  {path: 'logout', component: LogoutComponent},
  {path: 'security', component: SecurityComponent},
  {path: 'app', component: BodyComponent, children: [
    {path: 'encuesta', component: EncuestaComponent},
    {path: 'about', component: AboutComponent},
    {path: 'admin', component: AdminComponent, children: [
      {path: 'encuesta', component: TiponegocioComponent, children:[
        {path: 'modificar/:id' , component : ModificaEncuesta}
        ]},
   ]},
  ],  canActivate: [GuardService]
},
  {path: '**', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
