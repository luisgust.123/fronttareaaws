import { ActivatedRoute, Router, Params } from '@angular/router';
import { Encuesta } from '../../_model/encuesta';
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl } from '@angular/forms';
import { EncuestaService } from 'src/app/_services/encuestaService';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {

  id: number;
  form: FormGroup;
  encuesta: Encuesta;
  idGeneroSeleccionado: number;
  urlImagen: string;


  lenguaje: string;

  constructor( private route: ActivatedRoute , 
    private router: Router ,
     private encuestaService: EncuestaService,
     private snackBar: MatSnackBar) {  }

  ngOnInit() {

    this.fortInit();

  }

  fortInit(){
    this.form = new FormGroup({
      'idRol': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'edad': new FormControl(''),
      'profesion': new FormControl(''),
      'trabajo': new FormControl(''),
      'lenguaje': new FormControl(''),
      
    });
  }


  operar() {
    let encuesta = new Encuesta();
    encuesta.nombres = this.form.value['nombres'];
    encuesta.apellidos = this.form.value['apellidos'];
    encuesta.edad = this.form.value['edad'];
    encuesta.profesion = this.form.value['profesion'];
    encuesta.trabajo = this.form.value['trabajo'];
    encuesta.lenguaje = this.form.value['lenguaje'];

     this.encuestaService.guardar (encuesta).subscribe(() => {

      this.snackBar.open('Registro de Encuesta Exitosa, VUelva a llenar otra vez Encuestra', null, {
        duration: 1500,
      });
      this.fortInit();
     });

   }

  
}