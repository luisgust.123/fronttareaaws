import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBar, MatSort } from '@angular/material';
import { EncuestaService } from 'src/app/_services/encuestaService';
import { Encuesta } from 'src/app/_model/encuesta';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-adminEncuesta',
  templateUrl: './adminEncuesta.component.html',
  styleUrls: ['./adminEncuesta.component.css']
})
export class TiponegocioComponent implements OnInit {

  dataSource: MatTableDataSource<Encuesta>;
  totalElementos: number = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['id', 'nombres', 'apellidos', "lenguaje", 'acciones'];

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private encuestaService: EncuestaService,
    public route: ActivatedRoute
  ) {
   }

  ngOnInit() {
    this.cargarTabla();

    this.encuestaService.mensajeRegistro.subscribe((dato) => {
      this.dialog.closeAll();
      this.snackBar.open(dato, null, {
        duration: 1500,
      });
      this.cargarTabla();
    });
  }

  cargarTabla(){
    this.encuestaService.obtenerTodosLosRegistros().subscribe((datos) => {
      this.dataSource = new MatTableDataSource(datos);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  eliminar(id: number) {
    this.encuestaService.eliminar(id).subscribe((data) => {
      this.encuestaService.mensajeRegistro.next('Eliminado correctamente...');
    });
  }


}
