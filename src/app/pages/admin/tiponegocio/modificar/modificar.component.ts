import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { EncuestaService } from 'src/app/_services/encuestaService';
import { Encuesta } from 'src/app/_model/encuesta';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-modificar',
  templateUrl: './modificar.component.html',
  styleUrls: ['./modificar.component.css']
})
export class ModificaEncuesta implements OnInit {

  id: number;
  form: FormGroup;
  edicion: boolean = false;
  encuesta: Encuesta;
  idGeneroSeleccionado: number;
  urlImagen: string;


  lenguaje: string;

  constructor( private route: ActivatedRoute , private router: Router , private encuestaService: EncuestaService) {  }

  ngOnInit() {

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'edad': new FormControl(''),
      'profesion': new FormControl(''),
      'trabajo': new FormControl(''),
      'lenguaje': new FormControl(''),
      
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;
      this.initForm();
    });

  }

  initForm(){

   this.encuestaService.obtenerRegistros(this.id).subscribe(data => {

    this.form = new FormGroup({
      'id': new FormControl(data.id),
      'nombres': new FormControl(data.nombres),
      'apellidos': new FormControl(data.apellidos),
      'edad': new FormControl(data.edad),
      'profesion': new FormControl(data.profesion),
      'trabajo': new FormControl(data.trabajo),
      'lenguaje': new FormControl(data.lenguaje),
      
    });

   })

  }


  operar() {
    let encuesta = new Encuesta();
    encuesta.id = this.form.value['id'];
    encuesta.nombres = this.form.value['nombres'];
    encuesta.apellidos = this.form.value['apellidos'];
    encuesta.edad = this.form.value['edad'];
    encuesta.profesion = this.form.value['profesion'];
    encuesta.trabajo = this.form.value['trabajo'];
    encuesta.lenguaje = this.form.value['lenguaje'];

     this.encuestaService.guardar (encuesta).subscribe(() => {
         this.encuestaService.mensajeRegistro.next('Registro de Encuesta Exitosa');

         this.router.navigate(['/app/admin/encuesta']);  
        });

   }

}
